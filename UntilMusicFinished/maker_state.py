import random
import json
import os

from pico2d import *
import game_framework
import main_state
import pygame

pygame.mixer.init()

name = "MakerState" # 이 py파일의 이름은 제작자모드

MusicBackGroundImage = None # 백그라운드 이미지
AlbumArt = None # 앨범 아트 이미지
note_drop_screen = None # 노트 떨어지는 사각형 이미지
maker_info_screen = None # 노트 입력 정보를 보는 이미지
start_time = 0 # 노래 시작 시간 저장                   s
hit_time = 0 # 노트 입력 시간 저장
current_time = 0 # 현재 시간 저장

global file # 파일을 쓰기 위한 변수

class ImagingEffect:
    def __init__(self):
        self.a_image = load_image("image\\hitbox\\a.png")
        self.s_image = load_image("image\\hitbox\\s.png")
        self.j_image = load_image("image\\hitbox\\j.png")
        self.k_image = load_image("image\\hitbox\\k.png")


class UIScreen: # 이 클래스는 전반적인 유저 인터페이스를 나타냅니다
    def __init__(self):
        self.what_char_key_is = 0
        self.mode_name_font = load_font("font\\malgunbd.ttf", 40)
        self.hit_time_font = load_font('font\\malgunbd.ttf', 40)
        self.what_press_key_font = load_font('font\\malgunbd.ttf', 40)
        self.End_font = load_font("font\\malgunbd.ttf", 40)
        self.hit_time = 0.0
        self.play_time = 0.0

    def update(self):
        self.play_time = get_time() - start_time

    def draw(self):
        self.mode_name_font.draw(830, 625, "Maker Mode")
        self.what_press_key_font.draw(630, 450, "Playing Now Time : %f" % (self.play_time))
        self.hit_time_font.draw(630, 400, "Hit Time : %f" % (self.hit_time))
        self.what_press_key_font.draw(630, 350, "you press key : %c" % (self.what_char_key_is))
        self.End_font.draw(630, 150, "Exit : Esc")
        if self.what_char_key_is == 'a':
            effect.a_image.draw(105, 455)
            #self.what_press_key_font.draw(65, 60, "A hit")
        elif self.what_char_key_is == 's':
            effect.s_image.draw(235, 455)
            #self.what_press_key_font.draw(195, 60, "S hit")
        elif self.what_char_key_is == 'j':
            effect.j_image.draw(365, 455)
            #self.what_press_key_font.draw(325, 60, "J hit")
        elif self.what_char_key_is == 'k':
            effect.k_image.draw(495, 455)
            #self.what_press_key_font.draw(455, 60, "K hit")


class NoteDropScreen: # 이 클래스는 노트가 떨어지는 화면을 그리는 역할을 합니다
    def __init__(self):
        self.x = 300
        self.y = 50
        self.image = load_image("image\\back_main.jpg")

    def draw(self):
        self.image.draw(self.x, 360)

    def get_bb(self):
        return self.x - 255, self.y - 30, self.x + 255, self.y + 25

    def draw_bb(self):
        draw_rectangle(*self.get_bb())


class MakerInfoScreen: # 이 클래스는 노래의 앨범 아트를 그리는 역할을 하는데 그닥 필요없어보입니다
    def __init__(self):
        self.image = load_image("maker\\info.png")

    def draw(self):
        self.image.draw(930, 360)


def PlaySong(): # 노래 재생 함수
    global start_time
    pygame.mixer.music.load("maker\\returns_main.wav")
    pygame.mixer.music.play()
    start_time = get_time() #음악 시작 시간 저장
    pygame.mixer.music.set_volume(0.05)


def PauseSong(): # 노래 중지 함수
    pygame.mixer.music.pause()


def enter():
    global MusicBackGroundImage, note_drop_screen, UI, maker_info_screen, AlbumArt, effect
    MusicBackGroundImage = load_image("maker\\tutorial.png")
    AlbumArt = load_image("maker\\maker_main_art.png")
    note_drop_screen = NoteDropScreen()
    maker_info_screen = MakerInfoScreen()
    UI = UIScreen()
    PlaySong()
    effect = ImagingEffect()


def exit():
    global MusicBackGroundImage, note_drop_screen, UI, maker_info_screen, AlbumArt, effect
    del MusicBackGroundImage, note_drop_screen, UI, maker_info_screen, AlbumArt, effect


def pause():
    pass


def resume():
    pass


def handle_events(frame_time):
    global current_time, UI, file, start_time, hit_time, effect
    events = get_events()
    for event in events:
        if event.type == SDL_QUIT:
            game_framework.quit()
        elif event.type == SDL_KEYDOWN and event.key == SDLK_ESCAPE:
            PauseSong()
            game_framework.change_state(main_state)
        elif event.type == SDL_KEYDOWN and event.key == SDLK_a:
            UI.what_char_key_is = 'a'
            file = open("maker\\returns_data.txt", 'a')
            current_time = get_time() # 현재 시간 저장
            hit_time = (current_time - start_time) # 노트 입력한 시간 저장
            UI.hit_time = hit_time
            file.write('a ')
            file.write('%f' % hit_time)
            file.write('\n')
            print(hit_time)
            file.close()
        elif event.type == SDL_KEYDOWN and event.key == SDLK_s:
            UI.what_char_key_is = 's'
            file = open("maker\\returns_data.txt", 'a')
            current_time = get_time() # 현재 시간 저장
            hit_time = (current_time - start_time) # 노트 입력한 시간 저장
            UI.hit_time = hit_time
            file.write('s ')
            file.write('%f' % hit_time)
            file.write('\n')
            print(current_time - start_time)
            file.close()
        elif event.type == SDL_KEYDOWN and event.key == SDLK_j:
            UI.what_char_key_is = 'j'
            file = open("maker\\returns_data.txt", 'a')
            current_time = get_time() # 현재 시간 저장
            hit_time = (current_time - start_time) # 노트 입력한 시간 저장
            UI.hit_time = hit_time
            file.write('j ')
            file.write('%f' % hit_time)
            file.write('\n')
            print(current_time - start_time)
            file.close()
        elif event.type == SDL_KEYDOWN and event.key == SDLK_k:
            UI.what_char_key_is = 'k'
            file = open("maker\\returns_data.txt", 'a')
            current_time = get_time() # 현재 시간 저장
            hit_time = (current_time - start_time) # 노트 입력한 시간 저장
            UI.hit_time = hit_time
            file.write('k ')
            file.write('%f' % hit_time)
            file.write('\n')
            print(current_time - start_time)
            file.close()


def update(frame_time):
    global UI
    UI.update()


def draw(frame_time):
    global MusicBackGroundImage, note_drop_screen, maker_info_screen, UI, AlbumArt
    clear_canvas()
    MusicBackGroundImage.draw(640, 360)
    note_drop_screen.draw()
    #note_drop_screen.draw_bb()
    maker_info_screen.draw()
    UI.draw()
    AlbumArt.draw(1020, 150)
    update_canvas()