import game_framework
from pico2d import *
import pygame

name = "PauseState"
background_image = None
pause_image = None
pause_ment_image = None
counter= 0


def enter():
    global pause_image, pause_ment_image, background_image
    background_image = load_image("image\\pause_state2.png")
    pause_image = load_image("image\\pause.png")
    pause_ment_image = load_image("image\\pause_ment.png")


def exit():
    global pause_image,pause_ment_image, background_image
    del pause_image
    del pause_ment_image
    del background_image


def update(frame_time):
    global counter
    counter = (counter + 1) % 500


def draw(frame_time):
    global pause_image,pause_ment_image, background_image
    clear_canvas()
    background_image.draw(640, 360)
    if counter < 300:
        pause_image.draw(640, 500)
        pause_ment_image.draw(640, 180)
    update_canvas()


def handle_events(frame_time):
    events = get_events()
    for event in events:
        if event.type == SDL_QUIT:
            game_framework.quit()
        elif event.type == SDL_KEYDOWN and event.key == SDLK_p:
            pygame.mixer.music.unpause()
            # if key is p, return to previous state
            game_framework.pop_state()


def pause(): pass


def resume(): pass




