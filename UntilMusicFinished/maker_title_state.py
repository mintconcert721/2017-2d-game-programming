import title_state
import game_framework

from pico2d import *
import maker_state

name = "MakerTitleState"
maker_logo_image = None
maker_logo_time = 0.0


def enter():
    global maker_logo_image
    maker_logo_image = load_image("maker\\maker_mode_image.png")


def exit():
    global maker_logo_image
    del maker_logo_image


def update(frame_time):
    global maker_logo_time

    if maker_logo_time > 3.0:
        maker_logo_time = 0
        game_framework.push_state(maker_state) # 타이틀 진입
    delay(0.01)
    maker_logo_time += 0.01


def draw(frame_time):
    global maker_logo_image
    clear_canvas()
    maker_logo_image.draw(640, 360)
    update_canvas()


def handle_events(frame_time):
    events = get_events()


def pause(): pass


def resume(): pass




