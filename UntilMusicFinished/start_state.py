import title_state
import game_framework

from pico2d import *


name = "StartState"
logo_image = None
logo_time = 0.0


def enter():
    global logo_image
    open_canvas(1280, 720)
    logo_image = load_image("image\\GameLogo.png")


def exit():
    global logo_image
    del logo_image
    close_canvas()


def update(frame_time):
    global logo_time

    if logo_time > 1.0:
        logo_time = 0
        game_framework.push_state(title_state) # 타이틀 진입
    delay(0.01)
    logo_time += 0.01


def draw(frame_time):
    global logo_image
    clear_canvas()
    logo_image.draw(640, 360)
    update_canvas()


def handle_events(frame_time):
    events = get_events()


def pause(): pass


def resume(): pass




