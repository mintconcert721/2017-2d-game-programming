import game_framework
from pico2d import *
import main_state
import pygame

pygame.mixer.init()

name = "TitleState" # 이 py파일의 이름은 타이틀 상태

title_font_image = None # "아무 키나 누르십시요"를 띄우는 이미지 초기화
title_image = None # 타이틀 배경 이미지
title_music = None # 타이틀 음악

def PlaySong():
    pygame.mixer.music.load("bgm\\TitleBGM.wav")
    pygame.mixer.music.play(-1)
    pygame.mixer.music.set_volume(0.02)


def StopSong():
    pygame.mixer.music.stop()


class TitleFontImage: # 타이틀 폰트 클래스 -> 아무키나 누르십시요 출력하기 위함
    def __init__(self):
        self.image = load_image("image\\title\\introMessage.png")
        self.frame = 0

    def update(self):
        self.frame = self.frame + 0.005 #깜박 거리게 하기 위한 알고리즘

    def draw(self):
        if self.frame % 2 < 1:
            self.image.draw(1100, 550)


class TitleImage:
    def __init__(self):
        self.image = load_image("image\\title\\GameTitle.jpg")

    def draw(self):
        self.image.draw(640, 360)


def enter():
    global title_font_image, title_image
    title_font_image = TitleFontImage()
    title_image = TitleImage()
    PlaySong()


def exit():
    global title_font_image, title_image
    del title_font_image
    del title_image


def handle_events(frame_time):
    events = get_events()
    for event in events:
        if event.type == SDL_QUIT:
            game_framework.quit()
        else:
            if(event.type, event.key) == (SDL_KEYDOWN, SDLK_ESCAPE):
                StopSong()
                game_framework.quit()
            elif(event.type, event.key) == (SDL_KEYDOWN, SDLK_SPACE):
                StopSong()
                game_framework.change_state(main_state)


def update(frame_time):
    title_font_image.update()


def draw(frame_time):
    clear_canvas()
    title_image.draw()
    title_font_image.draw() # 타이틀 이미지 출력
    update_canvas()


def pause():
    pass


def resume():
    pass






