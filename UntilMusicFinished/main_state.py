import random
import json
import os

import pygame
from pico2d import *
import game_framework

import title_state
import blackpink_play_state
import produce101_play_state
import five_play_state
import SAO_play_state
import ncs_play_state
import mitis_play_state
import lol_play_state
import mc_the_max_play_state
import maker_title_state

pygame.mixer.init()

name = "MainState" # 이 py파일의 이름은 메인 상태

select_music_font = None
main_background = None
indicator_font = None
direction_key_indicator_font = None
maker_mode_image = None

global Album1, Album2, Album3, Album4, Album5, Album6, Album7, Album8, Album_list, Left, Right

CanUp = True # 앨범 아트 애니메이션을 위한 BOOLEAN 변수
CanLeftGo = True # 화살표 애니메이션을 위한 BOOLEAN 변수
CanGo = True # 화살표 애니메이션을 위한 BOOLEAN 변수


class AlbumListState: # 앨범 리스트의 상태
    def __init__(self):
        self.current_list_number = 1


class CAlbum4:
    def __init__(self):
        self.Album_art_image = load_image("playlist\\bpink\\BlackPink.png")
        self.name_image = load_image("playlist\\bpink\\BlackPink_name3.png")
        self.name_image.x = 900
        self.name_image.y = 300
        self.bgm = pygame.mixer.Sound("playlist\\bpink\\BlackPink_select.wav")

    def draw(self):
        self.Album_art_image.draw(300, 300)
        self.name_image.draw(self.name_image.x, self.name_image.y)

    def update(self):
        global CanUp
        if CanUp == True:
            self.up()
        elif CanUp == False:
            self.down()

    def up(self):
        global CanUp
        self.name_image.y += 0.1
        if self.name_image.y > 310:
            CanUp = False

    def down(self):
        global CanUp
        self.name_image.y -= 0.1
        if self.name_image.y < 290:
            CanUp = True

    def PlayMusic(self):
        #self.bgm.set_volume(0.1)
        self.bgm.set_volume(0.01)
        self.bgm.play()

    def StopMusic(self):
        self.bgm.stop()


class CAlbum2:
    def __init__(self):
        self.Album_art_image = load_image("playlist\\pickme\\pickme_art.jpg")
        self.name_image = load_image("playlist\\pickme\\pickme_name3.png")
        self.name_image.x = 900
        self.name_image.y = 300
        self.bgm = pygame.mixer.Sound("playlist\\pickme\\pickme_select.wav")

    def draw(self):
        self.Album_art_image.draw(300, 300)
        self.name_image.draw(self.name_image.x, self.name_image.y)

    def update(self):
        global CanUp
        if CanUp == True:
            self.up()
        elif CanUp == False:
            self.down()

    def up(self):
        global CanUp
        self.name_image.y += 0.1
        if self.name_image.y > 310:
            CanUp = False

    def down(self):
        global CanUp
        self.name_image.y -= 0.1
        if self.name_image.y < 290:
            CanUp = True

    def PlayMusic(self):
        #self.bgm.set_volume(0.1)
        self.bgm.set_volume(0.01)
        self.bgm.play()

    def StopMusic(self):
        self.bgm.stop()


class CAlbum7:
    def __init__(self):
        self.Album_art_image = load_image("playlist\\sao\\sao_art.png")
        self.name_image = load_image("playlist\\sao\\sao_name3.png")
        self.name_image.x = 900
        self.name_image.y = 300
        self.bgm = pygame.mixer.Sound("playlist\\sao\\swordland_select.wav")

    def draw(self):
        self.Album_art_image.draw(300, 300)
        self.name_image.draw(self.name_image.x, self.name_image.y)

    def update(self):
        global CanUp
        if CanUp == True:
            self.up()
        elif CanUp == False:
            self.down()

    def up(self):
        global CanUp
        self.name_image.y += 0.1
        if self.name_image.y > 310:
            CanUp = False

    def down(self):
        global CanUp
        self.name_image.y -= 0.1
        if self.name_image.y < 290:
            CanUp = True

    def PlayMusic(self):
        # self.bgm.set_volume(0.1)
        self.bgm.set_volume(0.01)
        self.bgm.play()

    def StopMusic(self):
        self.bgm.stop()


class CAlbum5:
    def __init__(self):
        self.Album_art_image = load_image("playlist\\apink\\APink_art.png")
        self.name_image = load_image("playlist\\apink\\APink_name3.png")
        self.name_image.x = 900
        self.name_image.y = 300
        self.bgm = pygame.mixer.Sound("playlist\\apink\\APink_select.wav")

    def draw(self):
        self.Album_art_image.draw(300, 300)
        self.name_image.draw(self.name_image.x, self.name_image.y)

    def update(self):
        global CanUp
        if CanUp == True:
            self.up()
        elif CanUp == False:
            self.down()

    def up(self):
        global CanUp
        self.name_image.y += 0.1
        if self.name_image.y > 310:
            CanUp = False

    def down(self):
        global CanUp
        self.name_image.y -= 0.1
        if self.name_image.y < 290:
            CanUp = True

    def PlayMusic(self):
        # self.bgm.set_volume(0.1)
        self.bgm.set_volume(0.01)
        self.bgm.play()

    def StopMusic(self):
        self.bgm.stop()


class CAlbum6:
    def __init__(self):
        self.Album_art_image = load_image("playlist\\ncs\\ncs_art.png")
        self.name_image = load_image("playlist\\ncs\\ncs_name3.png")
        self.name_image.x = 900
        self.name_image.y = 300
        self.bgm = pygame.mixer.Sound("playlist\\ncs\\ncs_select.wav")

    def draw(self):
        self.Album_art_image.draw(300, 300)
        self.name_image.draw(self.name_image.x, self.name_image.y)

    def update(self):
        global CanUp
        if CanUp == True:
            self.up()
        elif CanUp == False:
            self.down()

    def up(self):
        global CanUp
        self.name_image.y += 0.1
        if self.name_image.y > 310:
            CanUp = False

    def down(self):
        global CanUp
        self.name_image.y -= 0.1
        if self.name_image.y < 290:
            CanUp = True

    def PlayMusic(self):
        # self.bgm.set_volume(0.1)
        self.bgm.set_volume(0.01)
        self.bgm.play()

    def StopMusic(self):
        self.bgm.stop()


class CAlbum1:
    def __init__(self):
        self.Album_art_image = load_image("playlist\\mitis\\mitis.png")
        self.name_image = load_image("playlist\\mitis\\mitis_name3.png")
        self.name_image.x = 900
        self.name_image.y = 300
        self.bgm = pygame.mixer.Sound("playlist\\mitis\\born_select.wav")

    def draw(self):
        self.Album_art_image.draw(300, 300)
        self.name_image.draw(self.name_image.x, self.name_image.y)

    def update(self):
        global CanUp
        if CanUp == True:
            self.up()
        elif CanUp == False:
            self.down()

    def up(self):
        global CanUp
        self.name_image.y += 0.1
        if self.name_image.y > 310:
            CanUp = False

    def down(self):
        global CanUp
        self.name_image.y -= 0.1
        if self.name_image.y < 290:
            CanUp = True

    def PlayMusic(self):
        # self.bgm.set_volume(0.1)
        self.bgm.set_volume(0.01)
        self.bgm.play()

    def StopMusic(self):
        self.bgm.stop()


class CAlbum3:
    def __init__(self):
        self.Album_art_image = load_image("playlist\\lol\\lol.png")
        self.name_image = load_image("playlist\\lol\\lol_name3.png")
        self.name_image.x = 900
        self.name_image.y = 300
        self.bgm = pygame.mixer.Sound("playlist\\lol\\lol_select.wav")

    def draw(self):
        self.Album_art_image.draw(300, 300)
        self.name_image.draw(self.name_image.x, self.name_image.y)

    def update(self):
        global CanUp
        if CanUp == True:
            self.up()
        elif CanUp == False:
            self.down()

    def up(self):
        global CanUp
        self.name_image.y += 0.1
        if self.name_image.y > 310:
            CanUp = False

    def down(self):
        global CanUp
        self.name_image.y -= 0.1
        if self.name_image.y < 290:
            CanUp = True

    def PlayMusic(self):
        # self.bgm.set_volume(0.1)
        self.bgm.set_volume(0.01)
        self.bgm.play()

    def StopMusic(self):
        self.bgm.stop()


class CAlbum8:
    def __init__(self):
        self.Album_art_image = load_image("playlist\\mc_the_max\\returns.png")
        self.name_image = load_image("playlist\\mc_the_max\\returns_name3.png")
        self.name_image.x = 900
        self.name_image.y = 300
        self.bgm = pygame.mixer.Sound("playlist\\mc_the_max\\returns_select.wav")

    def draw(self):
        self.Album_art_image.draw(300, 300)
        self.name_image.draw(self.name_image.x, self.name_image.y)

    def update(self):
        global CanUp
        if CanUp == True:
            self.up()
        elif CanUp == False:
            self.down()

    def up(self):
        global CanUp
        self.name_image.y += 0.1
        if self.name_image.y > 310:
            CanUp = False

    def down(self):
        global CanUp
        self.name_image.y -= 0.1
        if self.name_image.y < 290:
            CanUp = True

    def PlayMusic(self):
        # self.bgm.set_volume(0.1)
        self.bgm.set_volume(0.01)
        self.bgm.play()

    def StopMusic(self):
        self.bgm.stop()


class LEFT:
    def __init__(self):
        self.image = load_image("image\\main\\left.png")
        self.image.x = 85
        self.image.y = 300

    def draw(self):
        self.image.draw(self.image.x, self.image.y)

    def update(self):
        global CanGo
        if CanGo == True:
            self.left_go()
        elif CanGo == False:
            self.right_go()

    def left_go(self):
        global CanGo
        self.image.x += 0.1
        if self.image.x > 85:
            CanGo = False

    def right_go(self):
        global CanGo
        self.image.x -= 0.1
        if self.image.x < 65:
            CanGo = True


class RIGHT:
    def __init__(self):
        self.image = load_image("image\\main\\right.png")
        self.image.x = 520
        self.image.y = 300

    def draw(self):
        self.image.draw(self.image.x, self.image.y)

    def update(self):
        global CanGo
        if CanGo == True:
            self.left_go()
        elif CanGo == False:
            self.right_go()

    def left_go(self):
        global CanGo
        self.image.x -= 0.1
        if self.image.x < 520:
            CanGo = True

    def right_go(self):
        global CanGo
        self.image.x += 0.1
        if self.image.x > 540:
            CanGo = False


def enter():
    global main_background, Album1, Album2, Album3, Album4, Album5, Album6, Album7, Album8
    global Album_list, Left, Right, indicator_font, direction_key_indicator_font, select_music_font, maker_mode_image
    main_background = load_image("image\\main\\MusicSelect.png")
    indicator_font = load_image("image\\main\\indicator.png")
    direction_key_indicator_font = load_image("image\\main\\VK.png")
    select_music_font = load_image("image\\main\\game_intro.png")
    maker_mode_image = load_image("image\\main\\into_maker.png")
    Album1 = CAlbum1()
    Album2 = CAlbum2()
    Album3 = CAlbum3()
    Album4 = CAlbum4()
    Album5 = CAlbum5()
    Album6 = CAlbum6()
    Album7 = CAlbum7()
    Album8 = CAlbum8()
    Left = LEFT()
    Right = RIGHT()
    Album_list = AlbumListState()


def exit():
    global main_background, Album1, Album2, Album3, Album4, Album5, Album6, Album7, Album8, Album_list
    global Left, Right, indicator_font, direction_key_indicator_font, select_music_font, maker_mode_image
    del main_background, Album1, Album2, Album3, Album4, Album5, Album6, Album7, Album8, Album_list
    del Left, Right, indicator_font, direction_key_indicator_font, select_music_font, maker_mode_image


def pause():
    pass


def resume():
    pass


def handle_events(frame_time):
    global Album_list
    events = get_events()
    for event in events:
        if event.type == SDL_QUIT:
            game_framework.quit()
        elif event.type == SDL_KEYDOWN and event.key == SDLK_ESCAPE:
            Album1.StopMusic()
            Album2.StopMusic()
            Album3.StopMusic()
            Album4.StopMusic()
            Album5.StopMusic()
            Album6.StopMusic()
            Album7.StopMusic()
            Album8.StopMusic()
            game_framework.change_state(title_state)
        elif event.type == SDL_KEYDOWN and event.key == SDLK_SPACE:
            Album1.StopMusic()
            Album2.StopMusic()
            Album3.StopMusic()
            Album4.StopMusic()
            Album5.StopMusic()
            Album6.StopMusic()
            Album7.StopMusic()
            Album8.StopMusic()
            if Album_list.current_list_number == 1:
                game_framework.change_state(mitis_play_state)
            elif Album_list.current_list_number == 2:
                game_framework.change_state(produce101_play_state)
            elif Album_list.current_list_number == 3:
                game_framework.change_state(lol_play_state)
            elif Album_list.current_list_number == 4:
                game_framework.change_state(blackpink_play_state)
            elif Album_list.current_list_number == 5:
                game_framework.change_state(five_play_state)
            elif Album_list.current_list_number == 6:
                game_framework.change_state(ncs_play_state)
            elif Album_list.current_list_number == 7:
                game_framework.change_state(SAO_play_state)
            elif Album_list.current_list_number == 8:
                game_framework.change_state(mc_the_max_play_state)
        elif event.type == SDL_KEYDOWN and event.key == SDLK_LEFT:
            if Album_list.current_list_number > 1:
                Album_list.current_list_number = Album_list.current_list_number - 1
        elif event.type == SDL_KEYDOWN and event.key == SDLK_RIGHT:
            if Album_list.current_list_number < 8:
                Album_list.current_list_number = Album_list.current_list_number + 1
        elif event.type == SDL_KEYDOWN and event.key == SDLK_END:
            Album1.StopMusic()
            Album2.StopMusic()
            Album3.StopMusic()
            Album4.StopMusic()
            Album5.StopMusic()
            Album6.StopMusic()
            Album7.StopMusic()
            Album8.StopMusic()
            game_framework.change_state(maker_title_state)


def update(frame_time):
    global MusicSelectImage, Album_list
    Left.update()
    Right.update()
    if Album_list.current_list_number == 1:
        Album1.PlayMusic()
        Album1.update()
        Album2.StopMusic()
        Album3.StopMusic()
        Album4.StopMusic()
        Album5.StopMusic()
        Album6.StopMusic()
        Album7.StopMusic()
        Album8.StopMusic()
    elif Album_list.current_list_number == 2:
        Album2.PlayMusic()
        Album2.update()
        Album1.StopMusic()
        Album3.StopMusic()
        Album4.StopMusic()
        Album5.StopMusic()
        Album6.StopMusic()
        Album7.StopMusic()
        Album8.StopMusic()
    elif Album_list.current_list_number == 3:
        Album3.PlayMusic()
        Album3.update()
        Album1.StopMusic()
        Album2.StopMusic()
        Album4.StopMusic()
        Album5.StopMusic()
        Album6.StopMusic()
        Album7.StopMusic()
        Album8.StopMusic()
    elif Album_list.current_list_number == 4:
        Album4.PlayMusic()
        Album4.update()
        Album1.StopMusic()
        Album2.StopMusic()
        Album3.StopMusic()
        Album5.StopMusic()
        Album6.StopMusic()
        Album7.StopMusic()
        Album8.StopMusic()
    elif Album_list.current_list_number == 5:
        Album5.PlayMusic()
        Album5.update()
        Album1.StopMusic()
        Album2.StopMusic()
        Album3.StopMusic()
        Album4.StopMusic()
        Album6.StopMusic()
        Album7.StopMusic()
    elif Album_list.current_list_number == 6:
        Album6.PlayMusic()
        Album6.update()
        Album1.StopMusic()
        Album2.StopMusic()
        Album3.StopMusic()
        Album4.StopMusic()
        Album5.StopMusic()
        Album7.StopMusic()
        Album8.StopMusic()
    elif Album_list.current_list_number == 7:
        Album7.PlayMusic()
        Album7.update()
        Album1.StopMusic()
        Album2.StopMusic()
        Album3.StopMusic()
        Album4.StopMusic()
        Album5.StopMusic()
        Album6.StopMusic()
        Album8.StopMusic()
    elif Album_list.current_list_number == 8:
        Album8.PlayMusic()
        Album8.update()
        Album1.StopMusic()
        Album2.StopMusic()
        Album3.StopMusic()
        Album4.StopMusic()
        Album5.StopMusic()
        Album6.StopMusic()
        Album7.StopMusic()



def draw(frame_time):
    global Album_list
    clear_canvas()
    main_background.draw(640, 360)
    Left.draw()
    Right.draw()
    indicator_font.draw(900, 90)
    direction_key_indicator_font.draw(300, 550)
    select_music_font.draw(180, 650)
    maker_mode_image.draw(1050, 650)
    if Album_list.current_list_number == 1:
        Album1.draw()
    elif Album_list.current_list_number == 2:
        Album2.draw()
    elif Album_list.current_list_number == 3:
        Album3.draw()
    elif Album_list.current_list_number == 4:
        Album4.draw()
    elif Album_list.current_list_number == 5:
        Album5.draw()
    elif Album_list.current_list_number == 6:
        Album6.draw()
    elif Album_list.current_list_number == 7:
        Album7.draw()
    elif Album_list.current_list_number == 8:
        Album8.draw()
    update_canvas()
