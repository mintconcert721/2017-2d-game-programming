import random
import json
import os

from pico2d import *
import game_framework
import main_state
import pause_state
import pygame

pygame.mixer.init()

name = "LolPlayState" # 이 py파일의 이름은 블랙핑크 플레이 스테이트

play_background_image = None # 백그라운드 배경 이미지
album_art_image = None # 앨범 아트 이미지
note_drop_screen = None # 노트 떨어지는 사각형 이미지
maker_info_screen = None # 노트 입력 정보를 보는 이미지
note_arr = None # 파일입출력을 통하여 저장되어 있는 데이터를 담기 위한 노트 리스트

AisClick = False# a키가 눌렸는지 확인하는 BOOLEAN 변수
SisClick = False # S키가 눌렸는지 확인하는 BOOLEAN 변수
JisClick = False # J키가 눌렸는지 확인하는 BOOLEAN 변수
KisClick = False # k키가 눌렸는지 확인하는 BOOLEAN 변수

a_hit_box = None  # 키 입력 시 노트 히트 판정 구간에 바운딩 박스를 그리는 객체
s_hit_box = None # 키 입력 시 노트 히트 판정 구간에 바운딩 박스를 그리는 객체
j_hit_box = None # 키 입력 시 노트 히트 판정 구간에 바운딩 박스를 그리는 객체
k_hit_box = None # 키 입력 시 노트 히트 판정 구간에 바운딩 박스를 그리는 객체

index = 0 # 노트 리스트를 탐색하기 위한 인덱스 변수
length = 0 # 음악 파일의 전체 크기를 담기 위한 변수

global file, UI
global check_time, start_time, effect


class ImagingEffect:
    def __init__(self):
        self.a_image = load_image("image\\hitbox\\a.png")
        self.s_image = load_image("image\\hitbox\\s.png")
        self.j_image = load_image("image\\hitbox\\j.png")
        self.k_image = load_image("image\\hitbox\\k.png")


class UIScreen: # 이 클래스는 전반적인 유저 인터페이스를 나타냅니다
    global max_combo
    def __init__(self):
        self.score = 0
        self.what_char_key_is = 0
        self.combo = 0
        self.great_number = 0
        self.good_number = 0
        self.miss_number = 0
        self.score_font = load_font('font\\GIGI.ttf', 40)
        self.music_name_font = load_font("font\\malgunbd.ttf", 25)
        self.great_score = load_font('font\\GIGI.ttf', 40)
        self.good_score = load_font('font\\GIGI.ttf', 40)
        self.high_score = load_font('font\\GIGI.ttf', 40)
        self.hitFont = load_font('font\\malgunbd.ttf', 30)
        self.pause_select_font = load_font('font\\malgunbd.ttf', 40)
        self.what_press_key_font = load_font('font\\malgunbd.ttf', 40)
        self.time = 0.0

    def update(self, frame_time):
        self.time = get_time() - start_time
        self.score = self.great_number * 100 + self.good_number * 30

    def draw(self, frame_time):
        self.score_font.draw(880, 550, 'SCORE:  %d' % (self.score))
        self.music_name_font.draw(800, 625, "League Of Legends - Challengers")
        self.great_score.draw(630, 450, "GREAT: %d" % (self.great_number))
        self.good_score.draw(630, 390, "GOOD: %d" % (self.good_number))
        self.high_score.draw(630, 300, "HIGH SCORE: %d" % (49800))
        #self.pause_select_font.draw(650, 160, "PAUSE : 'P'")
        if self.what_char_key_is == 'a':
            effect.a_image.draw(105, 455)
            # self.what_press_key_font.draw(65, 60, "A hit")
        elif self.what_char_key_is == 's':
            effect.s_image.draw(235, 455)
            # self.what_press_key_font.draw(195, 60, "S hit")
        elif self.what_char_key_is == 'j':
            effect.j_image.draw(365, 455)
            # self.what_press_key_font.draw(325, 60, "J hit")
        elif self.what_char_key_is == 'k':
            effect.k_image.draw(495, 455)
            # self.what_press_key_font.draw(455, 60, "K hit")


class Note:
    note_image = None
    def __init__(self):
        self.x = 0
        self.y = 0
        self.arr = []

    def insert(self, char, time):
        self.arr.append([char, time])
        if char == 'a':
            if self.note_image == None:
                self.note_image = load_image("note\\note1.png")
            self.x = 105
            self.y = 800
        elif char == 's':
            if self.note_image == None:
                self.note_image = load_image("note\\note2.png")
            self.x = 235
            self.y = 800
        elif char == 'j':
            if self.note_image == None:
                self.note_image = load_image("note\\note3.png")
            self.x = 365
            self.y = 800
        elif char == 'k':
            if self.note_image == None:
                self.note_image = load_image("note\\note4.png")
            self.x = 495
            self.y = 800

    def update(self, frame_time):
        self.y -= frame_time * 800

    def draw(self, frame_time):
        self.note_image.clip_draw(0, 0, 120, 120, self.x, self.y)

    def get_bb(self):
        return self.x - 60, self.y - 10, self.x + 60, self.y + 10

    def draw_bb(self):
        draw_rectangle(*self.get_bb())


class NoteDropScreen: # 이 클래스는 노트가 떨어지는 화면을 그리는 역할을 합니다
    def __init__(self):
        self.x = 300
        self.y = 50
        self.image = load_image("image\\back_main.jpg")

    def draw(self):
        self.image.draw(self.x, 360)

    def get_bb(self):
        return self.x - 255, self.y - 30, self.x + 255, self.y + 25

    def draw_bb(self):
        draw_rectangle(*self.get_bb())


class AHitBox:
    def __init__(self):
        self.x = 105
        self.y = 50

    def get_bb(self):
        return self.x - 60, self.y - 30, self.x + 60, self.y + 60

    def draw_bb(self):
        draw_rectangle(*self.get_bb())


class SHitBox:
    def __init__(self):
        self.x = 235
        self.y = 50

    def get_bb(self):
        return self.x - 60, self.y - 30, self.x + 60, self.y + 60

    def draw_bb(self):
        draw_rectangle(*self.get_bb())


class JHitBox:
    def __init__(self):
        self.x = 365
        self.y = 50

    def get_bb(self):
        return self.x - 60, self.y - 30, self.x + 60, self.y + 60

    def draw_bb(self):
        draw_rectangle(*self.get_bb())


class KHitBox:
    def __init__(self):
        self.x = 495
        self.y = 50

    def get_bb(self):
        return self.x - 60, self.y - 30, self.x + 60, self.y + 60

    def draw_bb(self):
        draw_rectangle(*self.get_bb())


class MakerInfoScreen:
    def __init__(self):
        self.image = load_image("image\\info.png")

    def draw(self):
        self.image.draw(930, 360)


class Pause:
    def __init__(self):
        self.image = load_image("pause.png")
        self.frame = 0
        self.printTime = 0

    def update(self):
        self.frame = self.frame + 0.005

    def draw(self):
        if self.frame % 2 < 1:
            self.image.draw(400, 400)


def collide(a, b):
    # fill here
    global UI
    left_a, bottom_a, right_a, top_a = a.get_bb() #바운딩 박스 (입력구간)
    left_b, bottom_b, right_b, top_b = b.get_bb() #바운딩 박스 (노트)

    if top_a < bottom_b: return False
    if bottom_a > top_b: return False
    if left_a > right_b: return False
    if right_a < left_b: return False

    if top_a > top_b and bottom_a < bottom_b:
        UI.great_number += 1
        return True

    if top_b > top_a and bottom_b < top_a:
        UI.good_number += 1
        return True
    elif top_b > bottom_a and bottom_b < bottom_a:
        UI.good_number += 1
        return True

    return True


def PlaySong():
    global start_time
    start_time = float(get_time())
    pygame.mixer.music.load("playlist\\lol\\challengers_main.wav")
    pygame.mixer.music.play()
    pygame.mixer.music.set_volume(0.1)


def PauseSong():
    pygame.mixer.music.pause()


def LengthOfFile():
    global length, line
    file = open("playlist\\lol\\lol_data.txt", 'r')
    length = 0
    line = None
    while True:
        line = file.readline()
        if not line:
            break
        length = length + 1
    file.close()
    return length


def enter():
    global play_background_image, note_drop_screen, UI, maker_info_screen, album_art_image, file, line, note_arr, index, a_hit_box, s_hit_box, j_hit_box, k_hit_box, effect
    file = open("playlist\\lol\\lol_data.txt", 'r')
    note_arr = [Note() for i in range(LengthOfFile())]
    line = None
    index = 0
    while True:
        line = file.readline()
        if not line: break
        note_arr[index].insert(line[0], float(line[2:]))
        index = index + 1
    file.close()
    #for i in range(len(note_arr)):
        #print(note_arr[i].arr[0][0])  # 어떤 노트인지 확인
        #print(note_arr[i].arr[0][1])  # 노트 나올 시간 확인

    PlaySong()
    play_background_image = load_image("playlist\\play_bg.png")

    a_hit_box = AHitBox()
    s_hit_box = SHitBox()
    j_hit_box = JHitBox()
    k_hit_box = KHitBox()

    note_drop_screen = NoteDropScreen()
    maker_info_screen = MakerInfoScreen()
    album_art_image = load_image("playlist\\lol\\lol_main_art.png")
    UI = UIScreen()
    effect = ImagingEffect()


def exit():
    global play_background_image, note_drop_screen, UI, maker_info_screen, album_art_image, a_hit_box, s_hit_box, j_hit_box, k_hit_box, effect
    del play_background_image, note_drop_screen, UI, maker_info_screen, album_art_image, a_hit_box, s_hit_box, j_hit_box, k_hit_box, effect


def pause():
    pass


def resume():
    pass


def handle_events(frame_time):
    global pauseFlag, UI, note_drop_screen, note_arr, AisClick, SisClick, JisClick, KisClick, max_combo, a_hit_box, s_hit_box, j_hit_box, k_hit_box, effect
    events = get_events()
    AisClick = False
    SisClick = False
    JisClick = False
    KisClick = False
    for event in events:
        if event.type == SDL_QUIT:
            game_framework.quit()
        elif event.type == SDL_KEYDOWN and event.key == SDLK_ESCAPE:
            del note_arr
            PauseSong()
            game_framework.change_state(main_state)
            # if p key goto pause state
            '''
        elif event.type == SDL_KEYDOWN and event.key == SDLK_p:
            PauseSong()
            game_framework.push_state(pause_state)'''
        if event.type == SDL_KEYDOWN and event.key == SDLK_a:
            UI.what_char_key_is = 'a'
            AisClick = True
            for note in note_arr:
                if collide(a_hit_box, note) and note.arr[0][0] == 'a':
                    note_arr.remove(note)
                    print("a collision")
        if event.type == SDL_KEYDOWN and event.key == SDLK_s:
            UI.what_char_key_is = 's'
            SisClick = True
            for note in note_arr:
                if collide(s_hit_box, note) and note.arr[0][0] == 's':
                    note_arr.remove(note)
                    print("s collision")
        if event.type == SDL_KEYDOWN and event.key == SDLK_j:
            UI.what_char_key_is = 'j'
            JisClick = True
            for note in note_arr:
                if collide(j_hit_box, note) and note.arr[0][0] == 'j':
                    note_arr.remove(note)
                    print("j collision")
        if event.type == SDL_KEYDOWN and event.key == SDLK_k:
            UI.what_char_key_is = 'k'
            KisClick = True
            for note in note_arr:
                if collide(k_hit_box, note) and note.arr[0][0] == 'k':
                    note_arr.remove(note)
                    print("k collision")


def update(frame_time):
    global note_arr, UI
    UI.update(frame_time)

    for note in note_arr:
        if (note.arr[0][1] - 1 <= float(UI.time)): # 노트가 알맞게 떨어지기 위한 알고리즘
            note.update(frame_time)

    if pygame.mixer.music.get_busy() == False: # 음악이 재생 중이지 않다면
        game_framework.change_state(main_state) # 메인 스테이트로 넘어가기


def draw(frame_time):
    global play_background_image, note_drop_screen, maker_info_screen, UI, album_art_image, note_arr, a_hit_box, s_hit_box, j_hit_box, k_hit_box

    clear_canvas()

    play_background_image.draw(640, 360)
    note_drop_screen.draw()
    maker_info_screen.draw()

    #a_hit_box.draw_bb()
    #s_hit_box.draw_bb()
    #j_hit_box.draw_bb()
    #k_hit_box.draw_bb()

    if AisClick == True:
        UI.hitFont.draw(80, 60, "HIT")
    elif SisClick == True:
        UI.hitFont.draw(210, 60, "HIT")
    elif JisClick == True:
        UI.hitFont.draw(340, 60, "HIT")
    elif KisClick == True:
        UI.hitFont.draw(470, 60, "HIT")

    UI.draw(frame_time)
    album_art_image.draw(1055, 150)

    for note in note_arr:
        note.draw(frame_time)
        #note.draw_bb()

    update_canvas()